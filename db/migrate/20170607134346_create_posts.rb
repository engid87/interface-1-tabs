class CreatePosts < ActiveRecord::Migration[5.1]
  def change
    create_table :posts do |t|
      t.string :author
      t.text :title
      t.integer :date
      t.text :body
      t.string :sns

      t.timestamps
    end
  end
end
