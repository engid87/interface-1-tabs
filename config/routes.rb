Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html


get '/', to: 'facebook_setting#index'
   get '/facebook_setting', to: 'facebook_setting#index'
   get '/facebook_setting/show', to: 'facebook_setting#show'
   get '/twitter_setting', to: 'twitter_setting#index'
   get '/twitter_setting/show', to: 'twitter_setting#show'
   get '/google_setting', to: 'google_setting#index'
   get '/google_setting/show', to: 'google_setting#show'
   get '/linkedin_setting', to: 'linkedin_setting#index'
   get '/linkedin_setting/show', to: 'linkedin_setting#show'
   get '/pinterest_setting', to: 'pinterest_setting#index'
   get '/pinterest_setting/show', to: 'pinterest_setting#show'
  get '/about', to: 'about#index'
  get '/example', to: 'example#index'

end
